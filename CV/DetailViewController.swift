//
//  DetailViewController.swift
//  CV
//
//  Created by Andreas Persson on 2019-12-05.
//  Copyright © 2019 Andreas Persson. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var theTitle: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    
    var imageM : UIImage?
    var thetitle : String?
    var descrip : String?
    var text : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.image = imageM
        theTitle.text = thetitle
        subTitle.text = descrip
        textLabel.text = text
        self.title = thetitle

    }

}
