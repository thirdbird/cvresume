//
//  ExperienceController.swift
//  CV
//
//  Created by Andreas Persson on 2019-11-05.
//  Copyright © 2019 Andreas Persson. All rights reserved.
//

import UIKit

struct Headline {
    var id : Int
    var title : String?
    var text : String?
    var image : String!
    var backtext : String?
}



class ExperienceController: UITableViewController {
    
    var headlines = [
        [Headline(id: 1, title: "Europe", text: "Play awesome and stay awesome.", image: "apple", backtext: "It's My Life This ain't a song for the broken-hearted No silent prayer for the faith-departed I ain't gonna be just a face in the crowd You're gonna hear my voice When I shout it out loud It's my life It's now or never I ain't gonna live forever I just want to live while I'm alive (It's my life) My heart is like an open highway Like Frankie said, I did it my way. I just wanna live while I'm alive It's my life This is for the ones who stood their ground For Tommy and Gina who never backed down Tomorrow's getting harder make no mistake Luck ain't even lucky Got to make your own breaks It's my life And it's now or never I ain't gonna live forever I just want to live while I'm alive"),
        Headline(id: 2, title: "Asia", text: "The world is our tour!", image: "banana", backtext: "It's My Life This ain't a song for the broken-hearted No silent prayer for the faith-departed I ain't gonna be just a face in the crowd You're gonna hear my voice When I shout it out loud It's my life It's now or never I ain't gonna live forever I just want to live while I'm alive (It's my life) My heart is like an open highway Like Frankie said, I did it my way. I just wanna live while I'm alive It's my life This is for the ones who stood their ground For Tommy and Gina who never backed down Tomorrow's getting harder make no mistake Luck ain't even lucky Got to make your own breaks It's my life And it's now or never I ain't gonna live forever I just want to live while I'm alive"),
        Headline(id: 3, title: "America", text: "Making a mark on our homefield.", image: "pineapple", backtext: "It's My Life This ain't a song for the broken-hearted No silent prayer for the faith-departed I ain't gonna be just a face in the crowd You're gonna hear my voice When I shout it out loud It's my life It's now or never I ain't gonna live forever I just want to live while I'm alive (It's my life) My heart is like an open highway Like Frankie said, I did it my way. I just wanna live while I'm alive It's my life This is for the ones who stood their ground For Tommy and Gina who never backed down Tomorrow's getting harder make no mistake Luck ain't even lucky Got to make your own breaks It's my life And it's now or never I ain't gonna live forever I just want to live while I'm alive")],
        [Headline(id: 1, title: "London", text: "Live at Piccadilly Circus", image: "pear", backtext: "It's My Life This ain't a song for the broken-hearted No silent prayer for the faith-departed I ain't gonna be just a face in the crowd You're gonna hear my voice When I shout it out loud It's my life It's now or never I ain't gonna live forever I just want to live while I'm alive (It's my life) My heart is like an open highway Like Frankie said, I did it my way. I just wanna live while I'm alive It's my life This is for the ones who stood their ground For Tommy and Gina who never backed down Tomorrow's getting harder make no mistake Luck ain't even lucky Got to make your own breaks It's my life And it's now or never I ain't gonna live forever I just want to live while I'm alive"),
        Headline(id: 2, title: "Budapest", text: "West vs east", image: "lemon", backtext: "It's My Life This ain't a song for the broken-hearted No silent prayer for the faith-departed I ain't gonna be just a face in the crowd You're gonna hear my voice When I shout it out loud It's my life It's now or never I ain't gonna live forever I just want to live while I'm alive (It's my life) My heart is like an open highway Like Frankie said, I did it my way. I just wanna live while I'm alive It's my life This is for the ones who stood their ground For Tommy and Gina who never backed down Tomorrow's getting harder make no mistake Luck ain't even lucky Got to make your own breaks It's my life And it's now or never I ain't gonna live forever I just want to live while I'm alive")
        ]
    ]
    
    var sections = ["Work", "Education"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.sections.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return headlines[section].count
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section]
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LabelCell") as! TableViewCell
        
        let section = headlines[indexPath.section][indexPath.row]
        cell.tableCellImage.image = UIImage(named: section.image)
        cell.tableCellTitle.text = section.title
        cell.tableCellSubTitle.text = section.text
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let didselectrowat = headlines[indexPath.section][indexPath.item]
        
        performSegue(withIdentifier: "detailSegue", sender: didselectrowat)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as? DetailViewController
        let postDestination : Headline? = sender as? Headline
        
        destination?.imageM = UIImage(named: postDestination?.image ?? "")
        destination?.thetitle = postDestination?.title
        destination?.descrip = postDestination?.text
        destination?.text = postDestination?.backtext
        
    }

}
