//
//  SkillsController.swift
//  CV
//
//  Created by Andreas Persson on 2019-10-30.
//  Copyright © 2019 Andreas Persson. All rights reserved.
//

import UIKit

class SkillsController: UIViewController {

    @IBOutlet weak var cloud1: UIImageView!
    @IBOutlet weak var cloud2: UIImageView!
    @IBOutlet weak var cloud3: UIImageView!
    @IBOutlet weak var cloud4: UIImageView!
    
    
    
    func moveIt(_ imageView: UIImageView,_  speed:CGFloat){
        let speeds = speed
        let imagespeed = speeds / view.frame.size.width
        let averageSpeed = (view.frame.size.width - imageView.frame.origin.x) * imagespeed
        
        UIView.animate(withDuration: TimeInterval(averageSpeed), delay: 0.0, options: .curveLinear, animations: {
            imageView.frame.origin.x = self.view.frame.size.width
        }, completion: { (_) in
            imageView.frame.origin.x = -imageView.frame.size.width
            self.moveIt(imageView, speeds)
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool){
        super.viewDidAppear(animated)
        
        moveIt(cloud1, 15)
        moveIt(cloud2, 5)
        moveIt(cloud3, 10)
        moveIt(cloud4, 7)
    }
    
    @IBAction func exitbutton_clicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    var image: UIImage?

    open var animationImages: [UIImage]? // nil by default
    
}
