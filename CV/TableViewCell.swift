//
//  TableViewCell.swift
//  CV
//
//  Created by Andreas Persson on 2019-12-11.
//  Copyright © 2019 Andreas Persson. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var tableCellImage: UIImageView!
    @IBOutlet weak var tableCellTitle: UILabel!
    @IBOutlet weak var tableCellSubTitle: UILabel!
    
}
